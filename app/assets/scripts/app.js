import $ from 'jquery';
import "lazysizes";
import "../styles/style.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";

//handles mobile Menu/Header
let mobileMenu = new MobileMenu();

//Handles Reveal On Scroll
new RevealOnScroll($("#our-beginning"));
new RevealOnScroll($("#departments"));
new RevealOnScroll($("#counters"));
new RevealOnScroll($("#testimonials"));


//Adding smooth scroll functionality to our header links
new SmoothScroll();

new ActiveLinks();

new Modal();

if(module.hot){
    module.hot.accept();
}